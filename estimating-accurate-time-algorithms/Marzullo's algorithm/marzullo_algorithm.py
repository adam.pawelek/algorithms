from copy import deepcopy


class TimePair():
    def __init__(self, type_of_task, time):
        self.type_of_task = type_of_task
        self.time = time

    def __str__(self):
        return (f"type of task {self.type_of_task} time {self.time}")


    def __lt__(self, other):
        if self.time == other.time:
            return self.type_of_task > other.type_of_task
        return self.time < other.time



class FinalTime():
    def __init__(self,start_time,end_time,layers):
        self.start_time = start_time
        self.end_time = end_time
        self.layers = layers

    def __eq__(self,other):
        if self.start_time == other.start_time and self.end_time == other.end_time and self.layers == other.layers:
            return True
        return False

    def __str__(self):
        return (f"start time{self.start_time} end time {self.end_time}  layers {self.layers}")
    def duration(self):
        return  self.startTime - self.end_time


def marzullo(clock_times,):
    algorithm_pairs = []
    max_time = FinalTime(0,0,0)
    currnet_time = FinalTime(0, 0,0)
    for i in range (len(clock_times)):
        put_to_list = TimePair(-1,clock_times[i][0])
        algorithm_pairs.append(put_to_list)
        put_to_list = TimePair(1,clock_times[i][1])
        algorithm_pairs.append(put_to_list)

    algorithm_pairs.sort()
    #for i in range (len (algorithm_pairs)):
    #    print(algorithm_pairs[i])
    for time_task in algorithm_pairs:
        if time_task.type_of_task == -1:
            currnet_time.start_time = time_task.time
            currnet_time.layers += 1
        if time_task.type_of_task == 1:
            currnet_time.end_time = time_task.time
            if currnet_time.layers > max_time.layers:
                max_time = deepcopy(currnet_time)
                #print (currnet_time)
            currnet_time.layers -= 1

    return (max_time)




def main():
    clock_times = [[3, 10], [1, 6], [4, 8], [6, 13], [9, 12]]
    search_time = marzullo(clock_times)
    print (search_time.layers)
    print(search_time.start_time)
    print(search_time.end_time)


if __name__ == '__main__':
    main()