from unittest import TestCase

from marzullo_algorithm import marzullo, FinalTime


class Test(TestCase):
    def test_marzullo_0(self):
        clock_times = [[3, 10], [1, 6], [4, 8], [6, 13], [9, 12]]
        search_time = marzullo(clock_times)
        correct_answear = FinalTime(4, 6, 3)
        self.assertEqual(search_time, correct_answear)

    def test_marzullo_border_test(self):
        clock_times = [[3, 10]]
        search_time = marzullo(clock_times)
        correct_answear = FinalTime(3, 10, 1)
        self.assertEqual(search_time, correct_answear)

    def test_marzullo_1(self):
        clock_times = [[0, 3],[2,4],[1,3],[3,4]]
        search_time = marzullo(clock_times)
        correct_answear = FinalTime(2, 3, 3)
        self.assertEqual(search_time, correct_answear)

    def test_marzullo_2(self):
        clock_times = [[0, 3],[2,4],[1,3],[3,4]]
        search_time = marzullo(clock_times)
        correct_answear = FinalTime(2, 3, 3)
        self.assertEqual(search_time, correct_answear)

    def test_marzullo_3(self):
        clock_times = [[0, 10],[2,8],[3,6],[20,30],[20,30],[20,30],[20,30]]
        search_time = marzullo(clock_times)
        correct_answear = FinalTime(20, 30, 4)
        self.assertEqual(search_time, correct_answear)

    def test_marzullo_4(self):
        clock_times = [[0, 1],[2,3],[4,6],[5,7]]
        search_time = marzullo(clock_times)
        correct_answear = FinalTime(5, 6, 2)
        self.assertEqual(search_time, correct_answear)




