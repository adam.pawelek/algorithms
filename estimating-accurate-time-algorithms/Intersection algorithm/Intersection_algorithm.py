from copy import deepcopy

from sortedcontainers import SortedList
sl = SortedList([5,4,3,2,1])

class TimePair():
    def __init__(self,type_of_task,middle_interval,time):
        self.type_of_task = type_of_task
        self.middle_interval = middle_interval
        self.time = time
    def __str__(self):
        return(f"type of task: {self.type_of_task}, middle_interval: {self.middle_interval}, time: {self.time}")


    def __lt__(self, other):
        if self.time == other.time:
            return self.type_of_task > other.type_of_task
        return self.time < other.time

class FinalTime():
    def __init__(self,start_time, end_time,layers):
        self.start_time = start_time
        self.end_time = end_time
        self.layers = layers
        self.duration = end_time - start_time
    def __eq__(self, other):
        if self.start_time == other.start_time and self.end_time == other.end_time and self.layers == other.layers:
            return True
        return False
    def __str__(self):
        return (f"start time: {self.start_time}, end time: {self.end_time}, layers: {self.layers}, duration: {self.duration}")



def intersection_algorithm(clock_times):
    algorithm_pairs = []
    for times in clock_times:
        middle_interval = round((times[0] + times[1])/ 2, 2)
        put_inside = TimePair(-1, middle_interval, times[0])
        algorithm_pairs.append(put_inside)
        put_inside = TimePair(1, middle_interval, times[1])
        algorithm_pairs.append(put_inside)


    algorithm_pairs.sort()
    #for i in range (len (algorithm_pairs)):
    #    print(algorithm_pairs[i])

    min_time = FinalTime(0,0,0)
    currnet_time = FinalTime(0, 0,0)
    middle_intervals = SortedList([])
    number_of_layers = 0
    for time_task in algorithm_pairs:
        if time_task.type_of_task == -1:
            number_of_layers+=1
            middle_intervals.add(time_task.middle_interval)
            currnet_time= FinalTime(middle_intervals[0],middle_intervals[-1],number_of_layers)
        if time_task.type_of_task == 1:
            number_of_layers-= 1
            middle_intervals.remove(time_task.middle_interval)
        if min_time.layers < currnet_time.layers:
            min_time = currnet_time
        else:
            if min_time.duration > currnet_time.duration:
                min_time = deepcopy(currnet_time)

    return min_time









def main():
    clock_times = [[3, 10], [1, 6], [4, 8], [6, 13], [9, 12]]
    search_time = intersection_algorithm(clock_times)
    print(search_time)
if __name__ == '__main__':
    main()