from unittest import TestCase

from Intersection_algorithm import FinalTime, intersection_algorithm


class Test(TestCase):
    def test_intersection_algorithm_0(self):
        right_answer = FinalTime(3.5,6.5,3)
        clock_times = [[3, 10], [1, 6], [4, 8], [6, 13], [9, 12]]
        search_time = intersection_algorithm(clock_times)
        self.assertEqual(right_answer,search_time)

    def test_intersection_algorithm_1(self):
        right_answer = FinalTime(2.5,3,2)
        clock_times = [[0, 2], [2, 4], [0, 5]]
        search_time = intersection_algorithm(clock_times)
        #print (search_time)
        self.assertEqual(right_answer,search_time)

    def test_intersection_algorithm_2(self):
        right_answer = FinalTime(1.5,3.5,3)
        clock_times = [[0, 3], [2, 5], [1, 6]]
        search_time = intersection_algorithm(clock_times)
        #print (search_time)
        self.assertEqual(right_answer,search_time)

    def test_intersection_algorithm_3(self):
        right_answer = FinalTime(6, 7.5,2)
        clock_times = [[0, 5], [4, 8], [6, 9]]
        search_time = intersection_algorithm(clock_times)
        #print (search_time)
        self.assertEqual(right_answer,search_time)

    def test_intersection_algorithm_4(self):#################
        right_answer = FinalTime(6, 7.5,2)
        clock_times = [[0, 5], [4, 8], [6, 9]]
        search_time = intersection_algorithm(clock_times)
        #print (search_time)
        self.assertEqual(right_answer,search_time)

    def test_intersection_algorithm_5(self):#################
        right_answer = FinalTime(5, 5.5 ,2)
        clock_times = [[0, 1],[1,4],[4,6],[2,5],[5,6]]
        search_time = intersection_algorithm(clock_times)
        #print (search_time)
        self.assertEqual(right_answer,search_time)

    def test_intersection_algorithm_border_test(self):
        right_answer = FinalTime(0.5,0.5,1)
        clock_times = [[0,1]]
        search_time = intersection_algorithm(clock_times)
        #print(search_time)
        self.assertEqual(right_answer,search_time)
